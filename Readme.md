This is an extended inscript keyboard which includes all malayalam charecters.
# install
Download the release file\
unzip\
press Cmd+Shift+G“, and type “/Library/Keyboard Layouts/” to navigate to the Keyboard Layouts, 
paste the poorna bundle here then add keyboard layout from keyboard settings.

# Use
The layout is a four layer one, that is a single key maps to a maximum of 4 characters. For example,

in case of 'd',\
'd' gives ക\
'Shift+d' gives ഖ\
'Right Alt+d' gives ഽ\
'Right Alt+Shift+d' gives ഁ

# Layout Image
![Image](poorna_remington.png)